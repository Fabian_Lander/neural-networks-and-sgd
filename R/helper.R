contains_na <- function(lst){
  for(i in lst){
    if(any(is.nan(lst[[i]]))) cat("matrix number ", i, " contains NAN \n")
  }
}

sigmoid <- function(x){
  return ( 1/(1 + exp((-1)*x)) )
}

d_sigmoid <- function(x){
  return( sigmoid(x)*(1-sigmoid(x)))
}

d_tanh <- function(x){
  return(1-(tanh(x)^2))
}

d_atan <- function(x){
  return(1/(x^2+1))
}

cost <- function(target, output){
  0.5*(dist(rbind(target, output), method = "euclidean")^2)[1]
}

cost_derivative <- function(output_activations, y){
  return(output_activations - y)
}

cost_func <- function(sample_outputs, sample_target){
  (1/length(sample_target))*sum(mapply(cost, sample_outputs, sample_target))
}
